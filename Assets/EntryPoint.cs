using Source.DI;
using UnityEngine;

namespace Source
{
    public class EntryPoint : MonoBehaviour
    {
        private void Awake()
        {
            new Context();
        }
    }
}