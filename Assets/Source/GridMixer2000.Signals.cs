using UnityEngine;

namespace Source
{
    public partial class GridMixer2000
    {
        private void OnModulationXSpeedChanged(int val)
        {
            modulationSpeed = new Vector2(val, modulationSpeed.y);
        }

        private void OnModulationYSpeedChanged(int val)
        {
            modulationSpeed = new Vector2(modulationSpeed.x, val);
        }

        private void OnGridSizeChanged(int val)
        {
            gridSize = val;
        }

        private void OnUseHeightChanged(bool val)
        {
            enableHeight = val;
        }

        private void OnAddNoiseChanged(bool val)
        {
            addNoise = val;
        }

        private void OnUseFilterChanged(bool val)
        {
            useNoiseFilter = val;
        }

        private void OnAmplitudeChanged(float val)
        {
            amplitude = val;
        }

        private void OnPlayBtnPressed(bool val)
        {
            _isPlaying = val;
        }
    }
}