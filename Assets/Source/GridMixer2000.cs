﻿using System;
using System.Collections.Generic;
using Source.DI;
using UnityEngine;
using Source.Signals;
using static UnityEngine.Matrix4x4;

namespace Source
{
    [InjectClass]
    public partial class GridMixer2000 : MonoBehaviour
    {
        private const int MaxMatrixCount = 100;
        private static readonly int CubeColor = Shader.PropertyToID("_Color");

        private readonly SinCosFunction _colorModulationFunction = new();

        #region UI_FIELDS

        public int gridSize = 100;
        public Vector2 modulationSpeed = Vector2.one;
        [SerializeField] private float amplitude = 2;
        [SerializeField] private bool enableHeight = true;
        [SerializeField] private bool addNoise;
        [SerializeField] private bool useNoiseFilter;

        #endregion

        [SerializeField] private GameObject cubePrefab;

        private int _currentGridSize;
        private Mesh _cubeMesh;
        private MeshRenderer _cubeRenderer;
        private List<Matrix4x4[]> _matrices = new(10);
        private MaterialPropertyBlock _props;
        private MaterialPropertyBlock _propertyBlock;
        private int _halfSize;
        private Bounds _bounds;
        private bool _isPlaying = true;

        // ReSharper disable once UnassignedGetOnlyAutoProperty
        // Value set via reflection
        [InjectProperty(typeof(SignalBus))] private SignalBus SignalBus { get; set; }

        private void Start()
        {
            UpdateGrid();
            _cubeMesh = cubePrefab.GetComponent<MeshFilter>().sharedMesh;
            _cubeRenderer = cubePrefab.GetComponent<MeshRenderer>();
            _propertyBlock = new MaterialPropertyBlock();
            SignalBus.Register(SignalBus.OnGridBoundsRecalculated, typeof(Bounds));

            SignalBus.Subscribe<int>(SignalBus.OnModulationXSpeedChanged, OnModulationXSpeedChanged);
            SignalBus.Subscribe<int>(SignalBus.OnModulationYSpeedChanged, OnModulationYSpeedChanged);
            SignalBus.Subscribe<float>(SignalBus.OnAmplitudeChanged, OnAmplitudeChanged);
            SignalBus.Subscribe<bool>(SignalBus.OnUseFilterChanged, OnUseFilterChanged);
            SignalBus.Subscribe<bool>(SignalBus.OnAddNoiseChanged, OnAddNoiseChanged);
            SignalBus.Subscribe<bool>(SignalBus.OnUseHeightChanged, OnUseHeightChanged);
            SignalBus.Subscribe<int>(SignalBus.OnGridSizeChanged, OnGridSizeChanged);
            SignalBus.Subscribe<bool>(SignalBus.OnPlayButtonPressed, OnPlayBtnPressed);
        }


        private void UpdateGrid()
        {
            _currentGridSize = gridSize;
            var collection = Math.Ceiling(_currentGridSize * _currentGridSize / (double) MaxMatrixCount);
            _matrices = new List<Matrix4x4[]>((int) collection);
            for (var index = 0; index < _matrices.Capacity; index++)
            {
                _matrices.Add(new Matrix4x4[MaxMatrixCount]);
            }

            _halfSize = _currentGridSize / 2;
        }

        private void Update()
        {
            if (_currentGridSize != gridSize)
            {
                UpdateGrid();
            }

            var firstPos = Vector3.zero;
            var lastPos = Vector3.zero;

            for (var i = 0; i < _currentGridSize; ++i)
            {
                if (!_isPlaying)
                {
                    break;
                }

                for (var j = 0; j < _currentGridSize; ++j)
                {
                    var color = new Color();
                    var ti = Time.time * modulationSpeed.x +
                             _colorModulationFunction.CycleLengthX * i / _currentGridSize;
                    var tj = Time.time * modulationSpeed.y +
                             _colorModulationFunction.CycleLengthY * j / _currentGridSize;
                    color.r = _colorModulationFunction.EvaluateX(ti);
                    color.g = _colorModulationFunction.EvaluateY(tj);
                    color.b = 0.5f;

                    //Instead of setting up color for each renderer, all data is passed directly to the shader
                    _propertyBlock.SetColor(CubeColor, color);

                    var pos = _cubeRenderer.transform.position + new Vector3(-_halfSize + i, 0, -_halfSize + j);

                    pos.y = 0;

                    if (enableHeight)
                    {
                        pos.y = (color.r + color.g) * amplitude;
                    }

                    if (addNoise)
                    {
                        pos.y += (Mathf.PerlinNoise(color.r, color.g) - 0.5f) * 2 * amplitude;
                    }

                    if (i == 0 && j == 0)
                    {
                        firstPos = pos;
                    }

                    if (i + 1 == _currentGridSize && j + 1 == _currentGridSize)
                    {
                        lastPos = pos;
                    }

                    //Since the sizes of the matrices might not coincide with the grid size, elements in the matrices are just stored in order. 
                    //Two lines of code below calculate which batch an element belongs to and what position it is in
                    var matrixIndex = (i * _currentGridSize + j) / MaxMatrixCount;
                    var elementIndex = (i * _currentGridSize + j) % MaxMatrixCount;

                    var isVisible = !useNoiseFilter ||
                                    Mathf.PerlinNoise(Time.time + color.r, Time.time + color.g) > 0.5f;

                    _matrices[matrixIndex][elementIndex] =
                        TRS(pos, Quaternion.identity, isVisible ? Vector3.one : Vector3.zero);
                }
            }

            var currentBounds = new Bounds((firstPos + lastPos) / 2, _currentGridSize * Vector3.one);
            if (_bounds.size != currentBounds.size)
            {
                _bounds = new Bounds(currentBounds.center, currentBounds.size);
                SignalBus.Invoke(SignalBus.OnGridBoundsRecalculated, currentBounds);
            }


            //Drawing batches of 100 cubes
            foreach (var matrix in _matrices)
            {
                Graphics.DrawMeshInstanced(_cubeMesh, 0, _cubeRenderer.sharedMaterial, matrix, MaxMatrixCount,
                    _propertyBlock);
            }
        }
    }
}