using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Source.Signals
{
    public struct SignalInfo
    {
        private readonly Type _argType;

        private Dictionary<int, Action<object>> subscribers;

        private SignalInfo(Type argType)
        {
            _argType = argType;
            subscribers = new Dictionary<int, Action<object>>(5);
        }

        public static SignalInfo Create(Type argType)
        {
            return new SignalInfo(argType);
        }

        public void AddSubscriber<T>(Action<T> newEvent)
        {
            Debug.Assert(typeof(T) == _argType,
                $"{newEvent.Method.Name}; Signal type mismatch. Expected: '{_argType.Name}', encountered '{typeof(T)}'");
            subscribers.Add(newEvent.GetHashCode(),
                arg =>
                {
                    newEvent(arg is T obj
                        ? obj
                        : throw new Exception($"{newEvent.Method.Name}; Failed to convert argument with type: '{arg.GetType()}' to type: '{typeof(T)}'"));
                });
        }

        public void RemoveSubscriber<T>(Action<T> newEvent)
        {
            subscribers.Remove(newEvent.GetHashCode());
        }

        public void Invoke(object eventArg)
        {
            foreach (var del in subscribers.Values)
            {
                del?.Invoke(eventArg);
            }
        }

        public void RemoveEvents()
        {
            subscribers.Clear();
        }
    }
}