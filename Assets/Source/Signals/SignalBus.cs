using System;
using System.Collections.Generic;
using Source.DI;

namespace Source.Signals
{
    [InjectClass]
    public partial class SignalBus
    {
        //Using the 'dynamic' keyword to save time. Only safe for small manageable projects with the .Net Framework API compatability configuration  
        private readonly Dictionary<string, SignalInfo> _events = new(5);

        public void Subscribe<T>(string name, Action<T> newEvent)
        {
            if (_events.TryGetValue(name, out var locatedEvent))
            {
                locatedEvent.AddSubscriber(newEvent);
                return;
            }

            throw new Exception($"Failed to locate an even by the name: {name}");
        }

        public void Unsubscribe<T>(string name, Action<T> newEvent)
        {
            if (_events.TryGetValue(name, out var locatedEvent))
            {
                locatedEvent.RemoveSubscriber(newEvent);
                return;
            }

            throw new Exception($"Failed to locate an even by the name: {name}");
        }

        public void Register(string eventName, Type argType)
        {
            if (!_events.TryAdd(eventName, SignalInfo.Create(argType)))
            {
                throw new Exception($"Already found an event with name: '{eventName}'");
            }
        }
        
        //Not actually required here
        public void Deregister(string name)
        {
            if (_events.TryGetValue(name, out var value))
            {
                value.RemoveEvents();
                _events.Remove(name);
                return;
            }

            throw new Exception($"Failed to locate an even by the name: {name}");
        }

        public void Invoke<T>(string name, T arg)
        {
            _events[name].Invoke(arg);
        }
    }
}