namespace Source.Signals
{
    public partial class SignalBus
    {
        public const string OnPlayButtonPressed = "OnPlayButtonPressed";
        public const string OnGridBoundsRecalculated = "OnGridBoundsRecalculated";
        public const string OnGridSizeChanged = "OnGridSizeChanged";
        public const string OnTopCamEnabled = "OnTopCamEnabled";
        public const string OnRotatingCamEnabled = "OnRotatingCamEnabled";
        public const string OnModulationXSpeedChanged = "OnModulationXSpeedChanged";
        public const string OnModulationYSpeedChanged = "OnModulationYSpeedChanged";
        public const string OnAmplitudeChanged = "OnAmplitudeChanged";
        public const string OnUseHeightChanged = "OnUseHeightChanged";
        public const string OnAddNoiseChanged = "OnAddNoiseChanged";
        public const string OnUseFilterChanged = "OnUseFilterChanged";
    }
}