using System.Text.RegularExpressions;
using Source.DI;
using Source.Signals;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Source
{
    [InjectClass]
    public class UIControls : MonoBehaviour
    {
        [SerializeField] private TMP_InputField _gridSize;
        [SerializeField] private Button _onTopCamEnabled;
        [SerializeField] private Button _onRotatingCamEnabled;
        [SerializeField] private Button _onPlayBtnPressed;
        [SerializeField] private TMP_InputField _modSpeedX;
        [SerializeField] private TMP_InputField _modSpeedY;
        [SerializeField] private Slider _amplitudeSlider;
        [SerializeField] private Toggle _useHeightToggle;
        [SerializeField] private Toggle _addNoiseToggle;
        [SerializeField] private Toggle _useFilterSlider;

        private bool _playBtnState = true;

        [InjectProperty(typeof(SignalBus))] private SignalBus SignalBus { get; set; }

        private void Start()
        {
            //Registering all UI events
            SignalBus.Register(SignalBus.OnGridSizeChanged, typeof(int));
            SignalBus.Register(SignalBus.OnTopCamEnabled, typeof(bool));
            SignalBus.Register(SignalBus.OnRotatingCamEnabled, typeof(bool));
            SignalBus.Register(SignalBus.OnModulationXSpeedChanged, typeof(int));
            SignalBus.Register(SignalBus.OnModulationYSpeedChanged, typeof(int));
            SignalBus.Register(SignalBus.OnAmplitudeChanged, typeof(float));
            SignalBus.Register(SignalBus.OnUseHeightChanged, typeof(bool));
            SignalBus.Register(SignalBus.OnAddNoiseChanged, typeof(bool));
            SignalBus.Register(SignalBus.OnUseFilterChanged, typeof(bool));
            SignalBus.Register(SignalBus.OnPlayButtonPressed, typeof(bool));

            //Subscribing to UI changes
            _onPlayBtnPressed.onClick.AddListener(() =>
            {
                _playBtnState = !_playBtnState;
                SignalBus.Invoke(SignalBus.OnPlayButtonPressed, _playBtnState);
            });

            _gridSize.onValueChanged.AddListener(val =>
            {
                var match = Regex.Match(val, @"[\d][\d]*");
                _gridSize.text = match.Success ? match.Value : string.Empty;

                if (int.TryParse(_gridSize.text, out var integer))
                {
                    SignalBus.Invoke(SignalBus.OnGridSizeChanged, integer);
                }
            });
            _onTopCamEnabled.onClick.AddListener(() => { SignalBus.Invoke(SignalBus.OnTopCamEnabled, true); });
            _onRotatingCamEnabled.onClick.AddListener(() =>
            {
                SignalBus.Invoke(SignalBus.OnRotatingCamEnabled, true);
            });
            _modSpeedX.onValueChanged.AddListener(val =>
            {
                var match = Regex.Match(val, @"[\d][\d]*");
                _modSpeedX.text = match.Success ? match.Value : string.Empty;
                if (int.TryParse(_modSpeedX.text, out var integer))
                {
                    SignalBus.Invoke(SignalBus.OnModulationXSpeedChanged, integer);
                }
            });
            _modSpeedY.onValueChanged.AddListener(val =>
            {
                var match = Regex.Match(val, @"[\d][\d]*");
                _modSpeedY.text = match.Success ? match.Value : string.Empty;
                if (int.TryParse(_modSpeedY.text, out var integer))
                {
                    SignalBus.Invoke(SignalBus.OnModulationYSpeedChanged, integer);
                }
            });
            _amplitudeSlider.onValueChanged.AddListener(val =>
            {
                SignalBus.Invoke(SignalBus.OnAmplitudeChanged, val);
            });

            _useHeightToggle.onValueChanged.AddListener(val =>
            {
                SignalBus.Invoke(SignalBus.OnUseHeightChanged, val);
            });

            _addNoiseToggle.onValueChanged.AddListener(val => { SignalBus.Invoke(SignalBus.OnAddNoiseChanged, val); });

            _useFilterSlider.onValueChanged.AddListener(val =>
            {
                SignalBus.Invoke(SignalBus.OnUseFilterChanged, val);
            });
        }
    }
}