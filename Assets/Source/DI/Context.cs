using System;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Source.DI
{
    public sealed class Context
    {
        /// <summary>
        /// Simple Dependency Injection solution for classes with a 'InjectClass' attribute 
        /// </summary>
        /// <exception cref="Exception"></exception>
        public Context()
        {
            //Picking up scripts from the main scene, placing them in a container
            var monoBehaviours = Object.FindObjectsOfType<Component>()
                .Where(x => x.GetType().GetCustomAttribute<InjectClass>() != null);
            var instances = monoBehaviours.ToDictionary(x => x.GetType(), y => (dynamic) y);
            //Gathering all non-mono classes
            var types = Assembly.GetAssembly(typeof(Context)).GetTypes().Where(type =>
                type.IsClass && !type.IsInterface && !type.IsSealed && !type.IsSubclassOf(typeof(MonoBehaviour)));

            var typesList = types.ToList();
            //Generating instances for non-mono classes
            foreach (var type in typesList.Where(type => type.GetCustomAttribute<InjectClass>() != null))
            {
                instances.Add(type, Activator.CreateInstance(type));
            }

            foreach (var type in instances.Keys)
            {
                if (!instances.TryGetValue(type, out var obj))
                {
                    throw new Exception($"Object of type {type.Name} has not been generated!");
                }

                //Injecting values into properties with an 'InjectProperty' attribute
                foreach (var field in type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic))
                {
                    var attribute = field.GetCustomAttribute<InjectProperty>();
                    if (attribute != null)
                    {
                        field.SetValue(obj, instances[attribute.InjectionType]);
                    }
                }
            }
        }
    }
}