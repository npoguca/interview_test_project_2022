using System;

namespace Source.DI
{
    internal class InjectProperty : Attribute
    {
        public Type InjectionType { get; }

        public InjectProperty(Type injectionType)
        {
            InjectionType = injectionType;
        }
    }
}