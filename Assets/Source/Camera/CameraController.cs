using Source.DI;
using Source.Signals;
using UnityEngine;

namespace Source.Camera
{
    [InjectClass]
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private UnityEngine.Camera _camera;
        private Bounds _bounds;

        private float _currentX;
        private float _currentY;
        private float _distance;
        private Vector3 _targetPosition;
        private Coroutine _coroutine;
        private WaitForEndOfFrame _waitForEndOfFrame;
        private bool _isRotatingCamEnabled;

        [InjectProperty(typeof(SignalBus))] private SignalBus SignalBus { get; set; }

        private void Start()
        {
            SignalBus.Subscribe<Bounds>(SignalBus.OnGridBoundsRecalculated, OnGridUpdated);
            SignalBus.Subscribe<bool>(SignalBus.OnTopCamEnabled, OnTopCamEnabled);
            SignalBus.Subscribe<bool>(SignalBus.OnRotatingCamEnabled, OnRotatingCamEnabled);
        }

        private void OnRotatingCamEnabled(bool _)
        {
            _currentX = 1;
            _currentY = 1;
            _camera.transform.position = GetRotationCamPosition();
            _isRotatingCamEnabled = true;
            _camera.transform.LookAt(_bounds.center);
        }

        private void Update()
        {
            if (!Input.GetMouseButton(0) || !_isRotatingCamEnabled)
            {
                return;
            }

            _currentX += Input.GetAxis("Mouse X");
            _currentY -= Input.GetAxis("Mouse Y");
            _camera.transform.position = GetRotationCamPosition();
            _camera.transform.LookAt(_bounds.center);
        }

        private Vector3 GetRotationCamPosition()
        {
            var matrix = Matrix4x4.Rotate(Quaternion.Euler(_currentY, _currentX, 0));
            var result = matrix.MultiplyVector(Vector3.one * _distance);
            return result;
        }

        private void OnGridUpdated(Bounds obj)
        {
            SetCamera(obj);
        }

        private void OnDestroy()
        {
            SignalBus.Unsubscribe<Bounds>(SignalBus.OnGridSizeChanged, OnGridUpdated);
        }

        private void SetCamera(Bounds bounds)
        {
            _bounds = bounds;
            _distance = bounds.size.z / Mathf.Tan(_camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
            if (_isRotatingCamEnabled)
            {
                OnRotatingCamEnabled(true);
                return;
            }

            OnTopCamEnabled(true);
        }

        private void OnTopCamEnabled(bool _)
        {
            var cameraTransform = _camera.transform;
            cameraTransform.rotation = Quaternion.Euler(90, 0, 0);
            cameraTransform.position = _bounds.center + Vector3.up * _distance;
            _isRotatingCamEnabled = false;
        }
    }
}