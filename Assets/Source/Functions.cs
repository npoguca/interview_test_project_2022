﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Source
{
    public interface INormalizedCyclicalFunction2d {

        float EvaluateX(float t);
        float EvaluateY(float t);

        (float x, float y) Evaluate(float t);

        float CycleLengthX {get;}
        float CycleLengthY {get;}
    }

    public class SinCosFunction : INormalizedCyclicalFunction2d {
        public float CycleLengthX => Mathf.PI*2;
        public float CycleLengthY => Mathf.PI*2;

        public (float x, float y) Evaluate(float t) {
            return (EvaluateX(t), EvaluateY(t));
        }

        public float EvaluateX(float t) {
            return Mathf.Sin(t)*0.5f + 0.5f;
        }

        public float EvaluateY(float t) {
            return Mathf.Cos(t)*0.5f + 0.5f;
        }

        // public (float min, float max) OuputRangeX => (-1, 1);
        // public (float min, float max) OuputRangeY => (-1, 1);
    }
}